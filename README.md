This is part of the Caldera Forms acceptance testing tool Ghost Runner. It is designed to run all tests of all suites, using a root URL -- provided by .env -- and assuming the URLs of the tests match the naming pattern documented in the Caldera Forms acceptance testing docs.

You may find this useful for other uses. That's cool, licenced is GPL, but main point of this is to make it easy to run all of our tests from one command, and control the URL.

## Install
### Requirements
* npm

### How To Install

* One install command to rule them all (including git clone and npm install)
```bash
git clone git@gitlab.com:caldera-labs/ghostrunner-runner-runner.git && cd ghostrunner-runner-runner && npm install
```

* Setup .env
    * Copy `.env-sample`'
    * Find API key [here](https://app.ghostinspector.com/account) or ask Josh.
    * Change value of APIKEY - Find

## Using
* Create a .env file
* Add a value for APIKEY and ROOTURL
* `npm run run`


## Adding Suites
When a new test suite is added in Ghost Inspector, it also needs to get added to the array SUITES.
