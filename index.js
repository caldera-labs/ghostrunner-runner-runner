#!/usr/bin/env node
"use strict";

const dotenv = require( 'dotenv' );
const CONFIG = dotenv.config({}).parsed;

const GhostInspector = require('ghost-inspector')(CONFIG.APIKEY);


const ROOTURL = undefined !== CONFIG.ROOTURL
	? CONFIG.ROOTURL
		: 'http://dev-cf-testing.pantheonsite.io/';

const lastPartOfUrl = function (url) {
	var parts = url.split('/');
	return  parts.pop() || parts.pop();
};

const SUITES = [
	//CF Field: Calculations
	'59d8091baea70072f47c435b',
	//CF Feature: Conditionals
	'59f538ec2c825860a04320bc',
	//CF Field: HTML Summary
	'59f3c05d8ba4c50f26eac7c6',
	//CF Field: Adv File
	'5a304bebbd6de962642a9d52',
	//CF Field: Credit Card
	'5a316b8cbd6de962642abe04',
	//CF Feature: Modals
	'5a0c958dd5628e428b35840a',
	//CF Feature: Edit Tokens
	'5a3abf00f2d3831d30cf5206',
	//CF Field: Select,
	'5a317e9fb96eed0c0d60ca9e',
	//CF Feature: Modals,
	'5a0c973b8ee17043538674e5'

];


SUITES.forEach( suiteId => {
	GhostInspector.getSuiteTests(suiteId, (err, tests) => {
		tests.forEach(test => {
            if( ! test.startUrl ){
                console.log( 'skipping test ' + test._id, test );
                return;
            }
			let startUrl = ROOTURL + lastPartOfUrl(test.startUrl);
			console.log( test._id,startUrl );
			GhostInspector.executeTest(test._id, {
				startUrl: startUrl
			}, (err, results, passing) => {
				if(err) return console.log(
					'Error: ' + err
				);
				console.log(results._id);
				console.log(passing === true ? 'Passed' : 'Failed');
			});
		});
	});
});

